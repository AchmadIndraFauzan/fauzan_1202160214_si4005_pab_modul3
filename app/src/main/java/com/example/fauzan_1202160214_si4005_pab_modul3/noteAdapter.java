package com.example.fauzan_1202160214_si4005_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class noteAdapter extends RecyclerView.Adapter<noteAdapter.noteViewHolder>{

    private Context context;
    private int mNumberItems;
    ArrayList<note> noteList;

    public noteAdapter(ArrayList<note> noteList, Context context, int num) {
        this.noteList = noteList;
        this.context = context;
        this.mNumberItems = num;
    }



    @NonNull
    @Override
    public noteAdapter.noteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        int layoutIdForNoteItem = R.layout.notes_layout;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForNoteItem, viewGroup, false);
        noteViewHolder viewHolder = new noteViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull noteAdapter.noteViewHolder noteViewHolder, int i) {
        note note = noteList.get(i);
        TextView nama = noteViewHolder.nama;
        TextView pekerjaan = noteViewHolder.pekerjaan;
        ImageView img = noteViewHolder.img;

        nama.setText(note.getNama());
        pekerjaan.setText(note.getPekerjaan());
        img.setImageResource(note.getImg());
        img.setTag(note.getTag());
    }

    @Override
    public int getItemCount() {
        mNumberItems = noteList.size();
        return mNumberItems;
    }

    public void deleteItem(int position) {
        noteList.remove(position);
        notifyItemRemoved(position);
    }
    public class noteViewHolder extends RecyclerView.ViewHolder {
        TextView nama, pekerjaan, kelamin;
        ImageView img;

        public noteViewHolder(@NonNull final View itemView){
            super(itemView);
            nama = itemView.findViewById(R.id.nama);
            pekerjaan = itemView.findViewById(R.id.pekerjaan);
            img = itemView.findViewById(R.id.img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), DetailContacts.class);
                    intent.putExtra("nama", nama.getText());
                    intent.putExtra("pekerjaan", pekerjaan.getText());
                    Object imageTag = img.getTag();
                    intent.putExtra("kelamin", String.valueOf(imageTag));
                    itemView.getContext().startActivity(intent);
                }
            });

        }


    }
}
