package com.example.fauzan_1202160214_si4005_pab_modul3;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DetailContacts extends AppCompatActivity {

    TextView nama, pekerjaan;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contacts);

        setTitle("Details");

        nama = findViewById(R.id.nama);
        pekerjaan = findViewById(R.id.pekerjaan);
        img = findViewById(R.id.img);

        nama.setText(getIntent().getStringExtra("nama"));
        pekerjaan.setText(getIntent().getStringExtra("pekerjaan"));

        String kelamin = getIntent().getStringExtra("kelamin");
        if(kelamin.equals("Male")){
            img.setImageDrawable(getResources().getDrawable(R.drawable.man));
        }else if(kelamin.equals("Female")){
            img.setImageDrawable(getResources().getDrawable(R.drawable.girl));
        }




    }
}
