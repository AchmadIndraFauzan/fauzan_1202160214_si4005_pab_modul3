package com.example.fauzan_1202160214_si4005_pab_modul3;

public class note {
    int img;
    String nama, pekerjaan, tag;

    public note(int img, String nama, String pekerjaan, String tag) {
        this.img = img;
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }
}
