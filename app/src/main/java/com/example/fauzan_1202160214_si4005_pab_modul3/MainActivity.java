package com.example.fauzan_1202160214_si4005_pab_modul3;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    private static final String[] dataKelamin = {"Male","Female"};
    private static Bundle bundle;
    FloatingActionButton fbTambah;
    Dialog addNote;
    String jk; //jenis kelamin
    String tag; //tag image
    int img; //image
    noteAdapter adapterNote;
    ArrayList<note> data;
    RecyclerView noteList;
    private Parcelable list = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = new ArrayList<>();
        noteList = (RecyclerView) findViewById(R.id.rv_list_note);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        noteList.setLayoutManager(layoutManager);


        noteList.setHasFixedSize(true);
        adapterNote = new noteAdapter(data,MainActivity.this, 20);

        noteList.setAdapter(adapterNote);

        fbTambah = findViewById(R.id.fbTambah);
        fbTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNote = new Dialog(MainActivity.this);
                addNote.setContentView(R.layout.activity_custom_dialog);

                final EditText etName = addNote.findViewById(R.id.etName);
                final EditText etPekerjaan = addNote.findViewById(R.id.etPekerjaan);

                //============ jenis kelamin =========
                Spinner jenisKelamin = addNote.findViewById(R.id.jenisKelamin);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_spinner_item,dataKelamin);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                jenisKelamin.setAdapter(adapter);
                jenisKelamin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position == 0){
                            jk = "Male";

                        }else if(position == 1){
                            jk = "Female";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                //============ btn submit ================
                Button btn_submit = addNote.findViewById(R.id.btn_submit);
                btn_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String nama = etName.getText().toString();
                        String pekerjaan = etPekerjaan.getText().toString();

                        if (jk == "Male"){
                            img = R.drawable.man;
                            tag = "Male";
                        }else if (jk == "Female"){
                            img = R.drawable.girl;
                            tag = "Female";
                        }

                        if(nama.equals("") || pekerjaan.equals("")){
                            Toast.makeText(MainActivity.this, "Masukkan data lengkap", Toast.LENGTH_SHORT).show();
                        }else{
                            data.add(new note(img, nama, pekerjaan, tag));
                            noteList.setAdapter(adapterNote);
                            addNote.dismiss();
                        }

                    }
                });
                //============ btn cancel ================
                Button btn_cancel = addNote.findViewById(R.id.btn_cancel);
                btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addNote.dismiss();
                    }
                });
                addNote.show();
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                Toast.makeText(MainActivity.this, "Swipe left to Delete", Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Toast.makeText(MainActivity.this, "Note Deleted", Toast.LENGTH_SHORT).show();
                int position = viewHolder.getAdapterPosition();
                data.remove(position);
                adapterNote.notifyDataSetChanged();

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(noteList);


    }
    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);

        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
            noteList.setLayoutManager(layoutManager);
        } else if (config.orientation == Configuration.ORIENTATION_PORTRAIT){
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            noteList.setLayoutManager(layoutManager);
        }
    }
}

